/*


computing 
external parameter  ray = external ray on the parameter plane
in = from infinity toward boundary of Mandelbrot set
using double precision
Newton method 

based on the code from mandelbrot-numerics library
by 	Claude Heiland-Allen : 
http://code.mathr.co.uk/mandelbrot-numerics
c/lib/m_d_exray_in.c
c/bin/m-exray-in.c


wikibooks : 
https://en.wikibooks.org/wiki/Fractals/mandelbrot-numerics

---------------------------------------------------------------

gcc m.c -lmpc -lmpfr -lgmp -lm -Wall

./a.out 
usage: ./a.out angle sharpness maxsteps


./a.out  1/3 4 5
./a.out  1/3 4 5 > c.txt


https://gitlab.com/adammajewski/m_d_exray_in


 git add m.c
 git commit -m "comments"
 git push -u origin master

*/
#include <stdio.h>
// #include <mandelbrot-numerics.h>
// #include "m-util.h"
#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h> //  strcmp
#include <errno.h> // 
#include <gmp.h>
#include <mpfr.h>
#include <mpc.h>


static inline int sgn(double z) {
  if (z > 0) { return  1; }
  if (z < 0) { return -1; }
  return 0;
}

static inline bool odd(int a) {
  return a & 1;
}

static inline double cabs2(double _Complex z) {
  return creal(z) * creal(z) + cimag(z) * cimag(z);
}

static inline bool cisfinite(double _Complex z) {
  return isfinite(creal(z)) && isfinite(cimag(z));
}

static const double pi = 3.141592653589793;
static const double twopi = 6.283185307179586;

// last . takeWhile (\x -> 2 /= 2 + x) . iterate (/2) $ 1 :: Double
static const double epsilon = 4.440892098500626e-16;

// epsilon^2
static const double epsilon2 = 1.9721522630525295e-31;



typedef struct  {
  mpq_t angle;
  mpq_t one;
  int sharpness;
  double er;
  double _Complex c;
  int j;
  int k;
}  m_d_exray_in;


enum m_newton { m_failed, m_stepped, m_converged };
typedef enum m_newton m_newton;



 m_d_exray_in *m_d_exray_in_new(const mpq_t angle, int sharpness) {
  m_d_exray_in *ray = malloc(sizeof(*ray));
  mpq_init(ray->angle);
  mpq_set(ray->angle, angle);
  mpq_init(ray->one);
  mpq_set_ui(ray->one, 1, 1);
  ray->sharpness = sharpness;
  ray->er = 65536.0;
  double a = twopi * mpq_get_d(ray->angle);
  ray->c = ray->er * (cos(a) + I * sin(a));
  ray->k = 0;
  ray->j = 0;
  return ray;
}

void m_d_exray_in_delete(m_d_exray_in *ray) {
  if (! ray) {
    return;
  }
  mpq_clear(ray->angle);
  mpq_clear(ray->one);
  free(ray);
}

m_newton m_d_exray_in_step(m_d_exray_in *ray, int maxsteps) {


/*


It's angle doubling modulo 1, 
which you need to do each time you cross a dwell band 
and t resets to ~Re^{2 pi i 2a} from ~sqrt(R)e^{2 pi i a}.

You need angle to be an exact rational, as floating point will lose
precision very quickly (53 iterations for double and you'll be constant 0).
*/

  if (ray->j >= ray->sharpness) {
    mpq_mul_2exp(ray->angle, ray->angle, 1); // angle *= 2
    if (mpq_cmp_ui(ray->angle, 1, 1) >= 0) { // if angle > 1
      mpq_sub(ray->angle, ray->angle, ray->one); // angle -= 1
    }
    ray->k = ray->k + 1;
    ray->j = 0;
  }
  
  
  double d = (ray->j + 0.5) / ray->sharpness;
  double r = pow(ray->er, pow(0.5,d ));
  double a = twopi * mpq_get_d(ray->angle);
  double _Complex target = r * (cos(a) + I * sin(a));
  double _Complex c = ray->c;
  
  
  
  
  
  for (int i = 0; i < maxsteps; ++i) {
    double _Complex z = 0;
    double _Complex dc = 0;
    for (int p = 0; p <= ray->k; ++p) {
      dc = 2 * z * dc + 1;
      z = z * z + c;
    }
    
    
    double _Complex c_new;
    double _Complex N =  (z - target) / dc;
    
    printf("k = %d j = %d d = %.16e dc = %.16e %.16e  r = %.16e t = %.16e %.16e N = %.16e %.16e\n", ray->k ,ray->j, d, creal(dc), cimag(dc), r, creal(target), cimag(target), creal(N), cimag(N));
    
    
    
    
     c_new = c - N;

    double d2 = cabs2(c_new - c);
    if (cisfinite(c_new)) {
      c = c_new;
    } else {
      break;
    }
    if (d2 <= epsilon2) {
      break;
    }
  }


  ray->j = ray->j + 1;
  
  
  
  
  double d2 = cabs2(c - ray->c);
  if (d2 <= epsilon2) {
    ray->c = c;
    
    return m_converged;
  }
  if (cisfinite(c)) {
    ray->c = c;
    return m_stepped;
  } else {
    return m_failed;
  }
}

double _Complex m_d_exray_in_get(const m_d_exray_in *ray) {
  if (! ray) {
    return 0;
  }
  return ray->c;
}

double _Complex m_d_exray_in_do(const mpq_t angle, int sharpness, int maxsteps, int maxnewtonsteps) {
  m_d_exray_in *ray = m_d_exray_in_new(angle, sharpness);
  if (! ray) {
    return 0;
  }
  for (int i = 0; i < maxsteps; ++i) {
    if (m_stepped != m_d_exray_in_step(ray, maxnewtonsteps)) {
      break;
    }
  }
  double _Complex endpoint = m_d_exray_in_get(ray);
  m_d_exray_in_delete(ray);
  return endpoint;
}


/* m-util.h */


static inline bool arg_rational(const char *arg, mpq_t x) {
  int ok = mpq_set_str(x, arg, 10);
  mpq_canonicalize(x);
  return ok == 0;
}



static inline bool arg_double(const char *arg, double *x) {
  char *check = 0;
  errno = 0;
  double d = strtod(arg, &check);
  if (! errno && arg != check && ! *check) {
    *x = d;
    return true;
  }
  return false;
}

static inline bool arg_int(const char *arg, int *x) {
  char *check = 0;
  errno = 0;
  long int li = strtol(arg, &check, 10);
  if (! errno && arg != check && ! *check) {
    *x = li;
    return true;
  }
  return false;
}


static void usage(const char *progname) {
  fprintf
    ( stderr
    , "usage: %s angle sharpness maxsteps\n"
    , progname
    );
}







int main(int argc, char **argv) {
  if (argc != 4) {
    usage(argv[0]);
    return 1;
  }
  bool native = true;
  
  
  mpq_t angle;
  mpq_init(angle);

  int sharpness = 0;
  int maxsteps = 0;

  if (! arg_rational(argv[1], angle)) { mpq_clear(angle); return 1; } else fprintf ( stderr , "arg_rational\n"  );
  if (! arg_int(argv[2], &sharpness)) { mpq_clear(angle); return 1; } else fprintf ( stderr , "arg_int sharpnes\n"  );
  if (! arg_int(argv[3], &maxsteps)) { mpq_clear(angle); return 1; }  else fprintf ( stderr , "arg_int maxsteps\n"  );


  int retval = 0;
  if (native) {
    m_d_exray_in *ray = m_d_exray_in_new(angle, sharpness);
    //printf("%.16e %.16e\n", creal(ray->c), cimag(ray->c));
    if (! ray) { mpq_clear(angle); return 1; }
    
    for (int i = 0; i < maxsteps; ++i) {
      complex double c = m_d_exray_in_get(ray);
      
   
   
      if (m_stepped != m_d_exray_in_step(ray, 8)) {
         printf("%.16e %.16e  m_stepped \n", creal(c), cimag(c));
         fprintf ( stderr , "m_stepped\n"  );
        retval = 1;
        break;
      }
    printf(" %.16e %.16e\n",  creal(c), cimag(c));  
      
    }
    m_d_exray_in_delete(ray);
  } else fprintf ( stderr , "more precision, use m_r_exray_in\n"  );
    
      
  
  mpq_clear(angle);
  return retval;
}
